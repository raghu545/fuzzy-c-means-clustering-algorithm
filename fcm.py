#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 2018

@author: Raghuveera Kori
"""

import numpy as np
from scipy.spatial.distance import cdist

import argparse

#updation of clusters
def update_clusters(x, u, m):
    um = u ** m
    v = um.dot(x) / np.atleast_2d(um.sum(axis=1)).T
  
    return um, v

#updation of membership
def fcm_function(x, v, m, metric):

    d = cdist(x, v, metric=metric).T

    # Sanitize Distances (Avoid Zeroes)
    d = np.fmax(d, np.finfo(np.float64).eps)

    exp = -2. / (m - 1)
    d2 = d ** exp
    u = d2 / np.sum(d2, axis=0, keepdims=1)
    return u, d

    
def fcm(x, c, m, e, max_iterations, metric="euclidean", v0=None):

    if not x.any() or len(x) < 1 or len(x[0]) < 1:
        print("Error: Data is in incorrect format")
        return

    # Num Features, Datapoints
    S, N = x.shape
    print("Datapoints: ",S)
    print("Features: ",N)
    if not c or c <= 0:
        print("Error: Number of clusters must be at least 1")

    if not m:
        print("Error: Fuzzifier must be greater than 1")
        return

    # Initialize the cluster centers
    # If the user doesn't provide their own starting points,
    if v0 is None:
        # Pick random values from dataset
        #xt = x.T

        v0 = x[np.random.choice(x.shape[0], int(c), replace=False), :]
        print('FCM Intial Centres:\n',v0)
    # List of all cluster centers (Bookkeeping)
    v = np.empty((max_iterations, int(c), N))

    v[0] = np.array(v0)

    # Membership Matrix Each Data Point in eah cluster
    u = np.zeros((max_iterations, int(c), S))
    
    # Number of Iterations
    t = 0

    while t < max_iterations - 1:

        u[t], d = fcm_function(x, v[t], m, metric)

        um,v[t + 1] = update_clusters(x, u[t], m)

        costfunction=um*d

        # Stopping Criteria
        if np.linalg.norm(v[t + 1] - v[t]) < e:

            break

        t += 1
        

    return v[t], v[0], u[t - 1], u[0], d, t,costfunction



def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-data', '--data1')
    
	#Initialize fuzziness coefficient, tolerance, iteration, clusters	
    parser.add_argument('-m', '--mvalue',type=float, default=2)
    parser.add_argument('-t', '--tolerance',type=float, default=1e-5)
    parser.add_argument('-itera', '--iteration',type=float, default=100)
    parser.add_argument('-c', '--clusters',type=int, default=2)
    
    parsed = parser.parse_args()
    
	#read iris data	
    data=np.loadtxt(parsed.data1, delimiter=',',usecols=(0,1,2,3))
    
    
    #print('data \n',data)
    print('Number Of Clusters: ',parsed.clusters)
    
    v,v0, ut, u0, d, t,costfunction =fcm(data,parsed.clusters, parsed.mvalue, parsed.tolerance, parsed.iteration, metric="euclidean", v0=None)

	#print centroid, membership, max iteration
    print('Centroid:\n',v)
    print('Membership:\n',ut.T)   
    print('Max_iteration: ',t)
    return 0

if __name__ == '__main__':
    main()
    
